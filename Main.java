package Arkanoid;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;


public class Main { 
    private static Draw draw;
    public static Timer timer;
    
    public static void main(String[] args) { 
        
        
        draw = new Draw(); // objeto de la clase Draw
        draw.setFocusTraversalKeysEnabled(false);
      
        // Escuchar al teclado dependiendo lo que haga ( sonido de teclas )
        draw.addKeyListener(new KeyListener(){

            @Override
            public void keyTyped(KeyEvent e) {
                
            }// primero

            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode() ){
                    default:
                        break;
                     case KeyEvent.VK_LEFT:  // Escuchamos el sonido de la tecla izquierdo
                        draw.Izquierda();
                        break;
                    case KeyEvent.VK_RIGHT:// Escuchamos el sonido de la tecla derecho
                       draw.Derecha();
                        break;
                }// Fin del Break
            } // intervencion

            @Override
            public void keyReleased(KeyEvent e) {
                
            }// final
        
        });
     /* Usamos el metod Timer que nos sirve para hacer repeticiones en cada cierto tiempo,
       aqui lo usamos para darle moviento a la pelota, con un movimiento cada .5 milisegundos  
      */
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
           
            @Override
            public void run(){
           draw.moverPelota();
            
            }
            
        },0,5 ); // fin del metodo Timer
        
       // Si el contador llega a 8 Gano el juego y muestra los puntos obtenidos
        
   /*
       if(draw.contador==8){
           JOptionPane.showMessageDialog(draw, "¡¡¡Ganaste!!!!");
           JOptionPane.showMessageDialog(draw, "Puntos "+draw.puntos);
       }
       */
   
    
    }
    
   
}//fin clase main
