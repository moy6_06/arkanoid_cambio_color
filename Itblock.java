package Arkanoid;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class Itblock{
    
    private Rectangle2D Rect;
    private boolean collied;
    private Color color;
    public int count;

    Color[] relleno = new Color[]{Color.LIGHT_GRAY,Color.GREEN,Color.PINK,Color.MAGENTA,Color.WHITE};
    
    
    public Itblock(double x,double y, double w,double h,Color color){
        
        Rect = new Rectangle2D.Double(x, y, w, h);
        this.color=color;
        this.collied=false;      //para saber si ya choco
        this.count=5;           // contador para ver cuantas vecces has pegado
    }//fin de clase public Itblock
    
    public Rectangle2D getRect(){
        return Rect;
        
    }

    public Color[] getmatriz(){

        return relleno;
    }
    
    public int getCount(){

        return count;
    }
    
    public void setCount(){

        this.count=count;
    }
    
    public Color getColor(){

        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        
    }
    
    public void setCollied(boolean collided){
      this.collied = collided;
    }
    
    public boolean iscollied(){
        return collied;
    }
    
    public boolean Intersects(Itblock other ){
          double x = this.getRect().getX();
        double a = other.getRect().getX();
        double y = this.getRect().getY();
        
    //accesede a la variable principal de otro objeto con el que se compara
    double b=other.getRect().getY();
    
    //accese a la variable principal del otro objetivo con el que se compara
    double w = this.getRect().getWidth();
    double c= other.getRect().getWidth();
    double h = this.getRect().getWidth();
    double d = other.getRect().getHeight();
    
         if(collided(x,y,w,h,a,b)  ||collided(x,y,w,h,a,(b+d))||collided(x,y,w,h,(a+c),(b+d))||collided(x,y,w,h,(a+c),b)||collided(a,b,c,d,(x+w),y)||collided(a,b,c,d,(x+w),(y+h))||collided(a,b,c,d,x,(y+h)))
         {
             
             return true;
             
         }
         
         
         else{
         return false;
         }
         
         
  }// fin de la clase Intersects
    
      public boolean collided(double x0,double y0,double w, double h ,double x, double y)
{
        return (x>=x0 && x<=x0+w && y>=y0 && y<=y0+h);
    }
    
    
}//fin de clase de Itblock