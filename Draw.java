package Arkanoid;


import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Draw extends JFrame {
    
    public Panel panel;
    private Rectangle rect;
    private int y=570,puntos=0;
    private int x=400,dir=0;
    private int x1=400,y1=500;
    private Itblock [] block = new Itblock[12];
    Itblock pelota=new Itblock(x1,y1,8,8,Color.BLACK);
    Itblock barra= new Itblock (x,570,120,10,Color.BLACK); ;
    public Itblock count;
    
   
    public Draw(){
        this.setSize(800, 600);
        rect = getBounds();
       // setResizable(true);
        setVisible(true);
        panel = new Panel();
        panel.setBackground(Color.CYAN);
        panel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED ) );
        this.setContentPane(panel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.pack();
    } //fin de clase Draw 
    
    
    public void Izquierda(){
        if(x>20) x-=60;
        panel.MoverBarra(x, 585);
        panel.repaint(); 
    }
    
    public void Derecha(){
        if(x<680) x+=60;
        panel.MoverBarra(x, 585);
        panel.repaint();
    }
    

     public void moverPelota(){
        
       // x = horizontal  y = vertical
       
          
         for (int i = 0; i <block.length  ; i++) {
           
                if (block[i].Intersects(pelota)) {
                    
        if(pelota.Intersects(block[i])&& dir==0 && block[i].count>0  ){
            dir=3; 
           block[i].count=block[i].count-1;  // a = a-1
           puntos+=10;
           
        }//fin del if 
        
        else if(pelota.Intersects(block[i])&& dir==1 && block[i].count>0){
            dir=2;
           block[i].count=block[i].count-1;  // a = -a1
           puntos+=10;
        }//fin del else
        
      
                
                }//fin del if de colicionar
             
             }//fin contador 
         
         
         if(dir==0)
           x1++;y1--; // x = horizontal  y = vertical
           
       if(x1==780 && dir==0)
           dir=1;
       
       if(dir==1){
        x1--; y1--;}  
       
        if(y1 <0 && dir==1){
           dir=2;}
        
          if(x1>=780){dir=2;}
          
        if(dir==2){
        x1-=2;y1+=2;    
        }
        
        if(x1<=0&&dir==1){
            dir=0;}else if(x1<=0){
            dir=3;}
        
        if(dir==3){
            x1+=2;y1+=2;}
                  
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   codigos de enviar direccion        
          if (pelota.Intersects(barra)&& dir==2 ) {
            dir=1;
        }
          else if (pelota.Intersects(barra) && dir==3) {
            dir=0 ;
        }
         
        if(y1>=580){
     //       if(v1==false)
            JOptionPane.showMessageDialog(rootPane,"Game OVer"); //Mostrar Game over
            JOptionPane.showMessageDialog(rootPane,"puntos : "+puntos );
            boolean v1=true;
            y1=10;
            
        }

         if(y1>580){
          // Main.timer.cancel();
       }
       panel.MoverPelota(x1, y1);
       panel.repaint();
        
     }// fin metodo pelota


   
    private class Panel extends JPanel{

        
    
     public Panel(){
     
         
         setPreferredSize(new Dimension(rect.width,rect.height));
         setResizable(false);
         
           for (int i = 0; i < block.length ; i++) {
         
               
             if(i>4 || i<9){
            block[i]= new Itblock((i-4)*200,10,190,60,Color.BLACK);
            
       
          }
           
           if(i>7){
                block[i]= new Itblock((i-8)*200,90,190,60,Color.black);
           }
           
           if(i<4){
               block[i]=new Itblock((i)*200,180,190,60,Color.BLACK );
           }
           
        }//fin de for de impresion
         
         
        
     }// fin del Panel
    
    @Override
    //aqui va switch
     protected void paintComponent(Graphics g){
         super.paintComponent(g);
         Graphics2D g2D = (Graphics2D)g;

         g2D.setBackground(Color.LIGHT_GRAY);
         g2D.setColor(Color.BLACK);
         g2D.drawString("puntos", 20,580);
       
         g2D.setColor(barra.getColor());
         g2D.fill(barra.getRect());
         g2D.setColor(pelota.getColor());
         g2D.fill(pelota.getRect());
         
         
         for (int i = 0; i <block.length ; i++) {

         if(block[i].getCount()>0 ){
             
             switch(block[i].getCount() ) {
                  case 5:
                 g2D.setColor(block[i].getmatriz()[0]);
                 g2D.fill(block[i].getRect());
                break;
              case 4:
                 g2D.setColor(block[i].getmatriz()[1]);
                 g2D.fill(block[i].getRect());
                break;
                case 3:
                 g2D.setColor(block[i].getmatriz()[2]);
                 g2D.fill(block[i].getRect());
                break;
                case 2:
                 g2D.setColor(block[i].getmatriz()[3]);
                 g2D.fill(block[i].getRect());
                break;
                case 1:
                 g2D.setColor(block[i].getmatriz()[4]);
                 g2D.fill(block[i].getRect());
                break;
                
                
            }// fin de switch
         }// fin del if         
            
        }//fin del for 
       
         
     }//fin paintComponent
     
     
     
     public void MoverBarra(double x, double y ){
        barra.getRect().setRect(x, 570, 120, 10);
    
     }
     
     public void MoverPelota(double x1, double y1){
     pelota.getRect().setRect(x1, y1, 8, 8);
         
         
     }
     
     
    
}//fin clase JPanel
    

}// fin clase Draw 

